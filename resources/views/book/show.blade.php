@extends('layouts.app')
@section('content')
    <div>
        <h1>Lista de libro</h1>
        <table class="table">
            <thead>
                <th>Id</th>
                <th>Titulo</th>
                <th>Paginas</th>
                <th>Año</th>
                <th>Genero</th>
                <th>Usuario alta</th>
                <th>Autor/es </th>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $book->id }}</td>
                    <td> {{ $book->title }}</td>
                    <td> {{ $book->pages }}</td>
                    <td> {{ $book->year }}</td>
                    <td> {{ $book->gender->name }}</td>
                    <td> {{ $book->user_id }}</td>
                    <td>
                    @foreach ($book->authors as $author)
                        {{ $author->name }},

                        @endforeach
                    </td>
                </tr>
            </tbody>
           

        </table>
    </div>
@endsection('content')

    