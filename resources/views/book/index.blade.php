
@extends('layouts.app')
@section('content')
    <div style='padding-left: 40px'>
        <h1>Lista de libros</h1>
         <div style='float:right;padding-right: 32%;'>
            @if (Auth::guest())
             <a href="/login">
                <button>Crear libro </button>
            </a>  
            @else
                <a href="/books/create">
                <button>Crear libro </button>
            </a>
            @endif



        </div>


        <table class="table">
            <thead>
                <th>Id</th>
                <th>Titulo</th>
                <th>Género</th>
                <th>Usuario alta</th>  
            </thead>
            <tbody>
            @foreach ($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->gender->name }}</td>
                    <td>{{ $book->user_id }}</td>
                    
                    <td>
                        <form method="post" action="/books/{{ $book->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a href="/books/{{ $book->id }}">Ver</a>
                            @if(Auth::user()->id === $book->user_id)
                                <input type="submit" value="Borrar">
                            @endif
                        </form>
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
        
        <div style='padding-left: 30%;float:rigth'>{{ $books->render() }} </div>
</div>
@endsection('content')
