@extends('layouts.app')

@section('content')
<div style='padding-left: 40px'>
<h1>Crear libro</h1>
<div class="form">
<form action="/books" method="post">
    {{ csrf_field() }}

    <label>Titulo: </label>
        <label><input type="text" name="title "></label><br><br>
    { $errors->first('title') }}
    <label>Paginas: </label>
        <label><input type="text" name="pages" "></label><br><br>
    { $errors->first('pages') }}
    <label>Año: </label>
        <label><input type="text" name="year></label><br><br>
    <div class="form-group">
        <label>Genero: </label>
            @foreach($genders as $gender)
            <div class="radio">
              <label><input type="radio" name="gender_id" value="{{ $gender->id }}">{{ $gender->name }}</label>
            </div>
            @endforeach
        {{ $errors->first('gender') }}
    </div>
    <label>Usuario: </label>
        <label><input type="text" name="user_id" value="{{ $user->id }}" hidden>{{ $user->name }}</label><br><br>
    
    <div class="form-group">
        <input type="submit" value="Crear">
    </div>
</form>
</div>
</div>
@endsection('content')