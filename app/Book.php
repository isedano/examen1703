<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	protected $fillable = [
        'title', 'pages', 'year','gender_id', 'user_id'
    ];

    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }
    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }
}
